package ndf25.dstickerlocation.Data;

import java.io.Serializable;

public class LocationData implements Serializable {

    private String mName = "";
    private String mAddress = "";
    private float mRate = 0;
    private String mMemo = "";
    private String mKey = "";
    private Boolean mAttention = false;

    public LocationData() {
    }

    public LocationData(String name, String address, float rate, String memo, String key, Boolean mAttention) {
        this.mName = name;
        this.mAddress = address;
        this.mRate = rate;
        this.mMemo = memo;
        this.mKey = key;
        this.mAttention = false;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public void setRate(float mRate) {
        this.mRate = mRate;
    }

    public void setMemo(String mMemo) {
        this.mMemo = mMemo;
    }

    public void setKey(String key) {
        this.mKey = key;
    }

    public void setAttention(Boolean attention){
        this.mAttention = attention;
    }

    public String getName() {
        return mName;
    }

    public String getAddress() {
        return mAddress;
    }

    public float getRate() {
        return mRate;
    }

    public String getMemo() {
        return mMemo;
    }

    public String getKey() {
        return mKey;
    }

    public  Boolean getAttention(){
        return mAttention;
    }
}
