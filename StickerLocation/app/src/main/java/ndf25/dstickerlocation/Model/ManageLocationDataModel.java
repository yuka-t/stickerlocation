package ndf25.dstickerlocation.Model;

import java.util.ArrayList;
import java.util.HashMap;

import ndf25.dstickerlocation.Data.LocationData;

public class ManageLocationDataModel {

    private static final ManageLocationDataModel mInstance = new ManageLocationDataModel();
    private ArrayList<LocationData> mLocationDataList = new ArrayList<LocationData>();

    private ArrayList<HashMap<String, LocationData>> mLocationDataMapList = new ArrayList<HashMap<String, LocationData>>();

    /**
     * 自身のインスタンスを取得します
     *
     * @return 自身のインスタンス
     */
    public static ManageLocationDataModel getInstance() {
        return mInstance;
    }

    private ManageLocationDataModel() {
    }

    /**
     * 店情報をリストに追加します
     *
     * @param data 店情報
     */
    public void addData(LocationData data) {
        mLocationDataList.add(data);
    }

    /**
     * 店情報のリストを取得します
     *
     * @return 店情報リスト
     */
    public ArrayList<LocationData> getLocationDataList() {
        return mLocationDataList;
    }

    /**
     * 店情報のリストサイズを取得します
     *
     * @return 登録されている店の数
     */
    public int getListSize() {
        return mLocationDataList.size();
    }

    /**
     * 店情報のリストを初期化します
     */
    public void clearList() {
        mLocationDataList.clear();
    }

    /**
     * 指定されたインデックスのデータをリストから削除します
     *
     * @param index
     */
    public void removeData(int index) {
        mLocationDataList.remove(index);
    }
}
