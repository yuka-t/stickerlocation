package ndf25.dstickerlocation.View;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ndf25.dstickerlocation.Data.LocationData;
import ndf25.dstickerlocation.R;

/**
 * シンプルリストにデータを設定します。
 */
public class SimpleAdapter extends BaseAdapter {

    private static final String TAG = SimpleAdapter.class.getSimpleName();

    Context context;
    LayoutInflater layoutInflater = null;
    ArrayList<LocationData> locationDataArrayList;

    public SimpleAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return locationDataArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return locationDataArrayList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.location_list,parent,false);
        Log.d(TAG, locationDataArrayList.get(position).toString());
        ((TextView)convertView.findViewById(R.id.textView_locationName)).setText(locationDataArrayList.get(position).getName());

        return convertView;
    }

    @Override
    public long getItemId(int position) {
        Log.d(TAG, "getItemId");
        return (long)position;
    }

    public void SetList(ArrayList<LocationData> list) {
        this.locationDataArrayList = list;
    }
}
