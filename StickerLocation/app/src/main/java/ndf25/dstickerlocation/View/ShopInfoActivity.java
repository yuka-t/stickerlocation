package ndf25.dstickerlocation.View;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import ndf25.dstickerlocation.Data.LocationData;
import ndf25.dstickerlocation.Model.ManageLocationDataModel;
import ndf25.dstickerlocation.R;
import ndf25.dstickerlocation.UIDataContext;

public class ShopInfoActivity extends AppCompatActivity {

    // Viewパーツ
    CheckBox mAttentionRadioButton;
    Button mRegistButton;
    Button mUpdateButton;
    EditText mShopNameText;
    EditText mShopAddressText;
    EditText mShopMemoText;
    TextView mShopRatingText;
    RatingBar mShopRatingBar;
    ImageButton mDeleteButton;

    /**
     * 登録ボタンリスナー
     * DataBaseの登録を行います
     */
    View.OnClickListener mRegistButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mShopNameText.getText().toString().equals("")){
                Toast.makeText(ShopInfoActivity.this, "店名が未入力です。", Toast.LENGTH_LONG).show();
                return;
            }

            LocationData data = new LocationData();
            data.setName(mShopNameText.getText().toString());
            data.setAddress(mShopAddressText.getText().toString());
            data.setRate(mShopRatingBar.getRating());
            data.setMemo(mShopMemoText.getText().toString());
            String dataKey = LocalDateTime.now().format(UIDataContext.mDateFormat);
            data.setKey(dataKey);
            data.setAttention(mAttentionRadioButton.isChecked());

            // DataBaseへの登録
            DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
            myRef.child("shopList").child(data.getKey()).setValue(data);

            Toast.makeText(ShopInfoActivity.this, "登録が完了しました。", Toast.LENGTH_LONG).show();
            finish();
        }
    };

    /**
     * 更新ボタンリスナー
     * DataBaseの更新を行います
     */
    View.OnClickListener onUpdateButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // 更新された情報のkeyを取得
            int listIndex = (int) getIntent().getSerializableExtra(MainActivity.KEY_SHOP_INFO);
            String key = ManageLocationDataModel.getInstance().getLocationDataList().get(listIndex).getKey();

            // 更新後情報
            LocationData afterData = new LocationData();
            afterData.setName(mShopNameText.getText().toString());
            afterData.setAddress(mShopAddressText.getText().toString());
            afterData.setRate(mShopRatingBar.getRating());
            afterData.setMemo(mShopMemoText.getText().toString());
            afterData.setAttention(mAttentionRadioButton.isChecked());

            // DataBaseの更新
            DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
            myRef.child("shopList").child(key).setValue(afterData);

            Toast.makeText(ShopInfoActivity.this, "更新が完了しました。", Toast.LENGTH_LONG).show();
            finish();
        }
    };

    /**
     * 削除ボタンリスナー
     * DataBaseの削除を行います
     */
    View.OnClickListener onDeleteButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // 表示している情報のkeyを取得
            int listIndex = (int) getIntent().getSerializableExtra(MainActivity.KEY_SHOP_INFO);
            String key = ManageLocationDataModel.getInstance().getLocationDataList().get(listIndex).getKey();

            // DataBaseへの更新
            DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
            myRef.child("shopList").child(key).removeValue();

            Toast.makeText(ShopInfoActivity.this , "削除が完了しました。", Toast.LENGTH_LONG).show();
            finish();
        }
    };

    // 評価リスナー
    RatingBar.OnRatingBarChangeListener onRatingBarChangeListener = new RatingBar.OnRatingBarChangeListener() {
        @Override
        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
            mShopRatingText.setText(String.valueOf(rating));
        }
    };

    View.OnClickListener onAttentionListenear = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Boolean status = mAttentionRadioButton.isChecked();
            Toast.makeText(ShopInfoActivity.this , "Attention。", Toast.LENGTH_LONG).show();
        }
    };

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_info);

        initializeView();
        inialzeData();

        // リスナー
        mRegistButton.setOnClickListener(mRegistButtonClickListener);
        mUpdateButton.setOnClickListener(onUpdateButtonClickListener);
        mShopRatingBar.setOnRatingBarChangeListener(onRatingBarChangeListener);
        mDeleteButton.setOnClickListener(onDeleteButtonClickListener);
        mAttentionRadioButton.setOnClickListener(onAttentionListenear);
    }

    /**
     * Viewパーツを初期化します
     */
    private void initializeView() {
        // Attention関連
        mAttentionRadioButton = (CheckBox) findViewById(R.id.attentionCheckBox);

        // ボタン関連
        mRegistButton = (Button) findViewById(R.id.registButton);
        mUpdateButton = (Button) findViewById(R.id.upDateButton);
        mDeleteButton = (ImageButton) findViewById(R.id.deleteButton);

        boolean upDate = getIntent().getBooleanExtra(MainActivity.KEY_UPDATE_FLAG, false);
        if (upDate) {
            // 更新の場合
            mRegistButton.setVisibility(View.GONE);
        } else {
            // 登録の場合
            mUpdateButton.setVisibility(View.GONE);
            mDeleteButton.setVisibility(View.GONE);
        }

        // 入力ボックス関連
        mShopNameText = (EditText) findViewById(R.id.shopNameText);
        mShopAddressText = (EditText) findViewById(R.id.shopAddressText);
        mShopMemoText = (EditText) findViewById(R.id.shopMemoText);

        // RatingBar関連
        mShopRatingBar = (RatingBar) findViewById(R.id.shopRateRatingBar);
        mShopRatingText = (TextView) findViewById(R.id.shopRateText);
    }

    /**
     * データを初期化し、表示します
     */
    private void inialzeData() {
        int index = (int) getIntent().getIntExtra(MainActivity.KEY_SHOP_INFO, -1);
        if (index != -1) {
            LocationData data = ManageLocationDataModel.getInstance().getLocationDataList().get(index);
            if (data != null) {
                mShopNameText.setText(data.getName());
                mShopAddressText.setText(data.getAddress());
                mShopRatingBar.setRating(data.getRate());
                mShopRatingText.setText(String.valueOf(data.getRate()));
                mShopMemoText.setText(data.getMemo());
                mAttentionRadioButton.setChecked(data.getAttention());
            }
        }
    }
}
