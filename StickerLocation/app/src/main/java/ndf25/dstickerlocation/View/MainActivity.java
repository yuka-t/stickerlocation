package ndf25.dstickerlocation.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;

import ndf25.dstickerlocation.Data.LocationData;
import ndf25.dstickerlocation.Model.ManageLocationDataModel;
import ndf25.dstickerlocation.R;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    // Viewパーツ
    FloatingActionButton mAddButton;
    Toolbar mToolBar;
    SimpleAdapter mMyAdapter;
    ListView mListView;

    // 画面遷移時に情報を取得するために必要なKey一覧
    public static final String KEY_SHOP_INFO = "shopInfo";
    public static final String KEY_UPDATE_FLAG = "upDateFlag";

    ChildEventListener mPostListener = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            LocationData data = dataSnapshot.getValue(LocationData.class);
            ManageLocationDataModel.getInstance().addData(data);
            mMyAdapter.notifyDataSetChanged();
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            // 変更されたDataBaseの値を取得
            LocationData changeData = dataSnapshot.getValue(LocationData.class);

            // 現在のmodelの値を取得
            String key = dataSnapshot.getKey();
            LocationData currentData = null;
            for (int i = 0; i < ManageLocationDataModel.getInstance().getLocationDataList().size(); i++) {
                LocationData tempData = ManageLocationDataModel.getInstance().getLocationDataList().get(i);
                if (key != null && key.equals(tempData.getKey())) {
                    currentData = tempData;
                    break;
                }
            }

            // modelの値を新しい情報に更新
            if (changeData != null && currentData != null) {
                currentData.setName(changeData.getName());
                currentData.setAddress(changeData.getAddress());
                currentData.setRate(changeData.getRate());
                currentData.setMemo(changeData.getMemo());
                currentData.setAttention(changeData.getAttention());
                mMyAdapter.notifyDataSetChanged();
            } else {
                Log.d(TAG, "fail to change data.");
            }
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            // 変更された店情報のkeyを取得
            String key = dataSnapshot.getKey();

            // 削除された情報をmodelからも削除
            for (int i = 0; i < ManageLocationDataModel.getInstance().getLocationDataList().size(); i++) {
                LocationData tempData = ManageLocationDataModel.getInstance().getLocationDataList().get(i);
                if (key != null && key.equals(tempData.getKey())) {
                    ManageLocationDataModel.getInstance().removeData(i);
                    mMyAdapter.notifyDataSetChanged();
                    break;
                }
            }
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeView();

        // FireBaseリスナー
        FirebaseDatabase.getInstance().getReference("shopList").addChildEventListener(mPostListener);

        // 追加ボタンリスナー
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(), ShopInfoActivity.class);
                startActivity(intent);
            }
        });

        // ListViewリスナー
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplication(), ShopInfoActivity.class);
                //LocationData item = (LocationData)mListView.getItemAtPosition(position);
                intent.putExtra(KEY_SHOP_INFO, position);
                intent.putExtra(KEY_UPDATE_FLAG, true);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ManageLocationDataModel.getInstance().clearList();
    }

    public void initializeView() {
        // ツールバー関連
        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);

        // 追加ボタン関連
        mAddButton = (FloatingActionButton) findViewById(R.id.addButon);

        // リスト関連
        mMyAdapter = new SimpleAdapter(MainActivity.this);
        mMyAdapter.SetList(ManageLocationDataModel.getInstance().getLocationDataList());
        mListView = (ListView) findViewById(R.id.locationList);
        mListView.setAdapter(mMyAdapter);
    }

    public void initialize() {

    }


}
